from django.contrib.auth.mixins import LoginRequiredMixin
from django.urls import reverse
from django.views.generic import DetailView, UpdateView, RedirectView

from .models import Account


class AccountView(LoginRequiredMixin, DetailView):
    model = Account

    def get_object(self, queryset=None):
        if not self.kwargs.get('pk'):
            self.kwargs['pk'] = self.request.user.id
        return super().get_object(queryset)

    def get_context_data(self, **kwargs):
        context = super(AccountView, self).get_context_data(**kwargs)
        if 'account' in context and context['account'].social_auth.exists():
            try:
                context['github'] = context['account'].social_auth.get(provider='github')
            except Account.DoesNotExist:
                pass
        return context


class AccountUpdate(LoginRequiredMixin, UpdateView):
    model = Account
    fields = ('first_name', 'last_name', 'email')

    def get_success_url(self):
        return reverse('account:view')


class ProfileView(RedirectView):
    def get_redirect_url(self, *args, **kwargs):
        if self.request.user and self.request.user.is_authenticated:
            return reverse('account:view')
        return '/'
