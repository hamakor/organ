init:
	pipenv install

init-dev:
	pipenv install --dev

docker-devenv:
	docker-compose -f docker-compose-dev.yml up -d
