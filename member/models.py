from django.contrib.auth import get_user_model
from django.db import models
from djext.blame.models import BaseBlameModel


AccountModel = get_user_model()


class Member(BaseBlameModel):
    """
    A model of member

    member_id (int): An internal member id
    user: A link to a login user account (optional)
    first_name (str): The member's first name
    last_name (str): The member's last name
    id_type (str): Type of the ID document (usually National or passport)
    id_number (int): ID number of the member
    locale (str): Preferred locale of the member (
    """
    member_id = models.IntegerField(db_index=True)
    user = models.OneToOneField(AccountModel, blank=True, null=True, related_name='member')
    first_name = models.CharField(max_length=64)
    last_name = models.CharField(max_length=64)
    id_type = models.CharField(max_length=32)
    id_number = models.CharField(max_length=32)

    locale = models.CharField(max_length=5, null=True, blank=True)

    def get_absolute_url(self):
        return '/member/{id}/'.format(id=self.id)

    def get_full_name(self):
        """
        Returns the first_name plus the last_name, with a space in between.
        """
        full_name = '{first_name} {last_name}'.format(first_name=self.first_name, last_name=self.last_name)
        return full_name.strip()

    def get_short_name(self):
        """
        Returns the short name for the user.
        """
        return self.first_name

    def __str__(self):
        return self.get_full_name()


class MembershipType(models.Model):
    name = models.CharField(max_length=32)
    period = models.CharField(max_length=16, null=True, blank=True)

    def __str__(self):
        return self.name


class Membership(BaseBlameModel):
    member = models.ForeignKey(Member, related_name='memberships')
    membership_type = models.ForeignKey(MembershipType)
    start_date = models.DateField()
    end_date = models.DateField()

    def __str__(self):
        with self as membership:
            return '{membership.member} ({membership.membership_type})'.format(membership=membership)


class MemberContactDetail(BaseBlameModel):
    primary = models.BooleanField(default=False)
    active = models.BooleanField(default=True)

    class Meta:
        abstract = True


class MemberEmail(MemberContactDetail):
    member = models.ForeignKey(Member, related_name='emails')
    email = models.EmailField(max_length=255)

    def __str__(self):
        return self.email


class MemberPhone(MemberContactDetail):
    member = models.ForeignKey(Member, related_name='phones')
    phone = models.CharField(max_length=20)
    text_only = models.BooleanField(default=False)

    def __str__(self):
        return self.phone


class MemberAddress(MemberContactDetail):
    member = models.ForeignKey(Member, related_name='addresses')
    country = models.CharField(max_length=32)
    state = models.CharField(max_length=32)
    zip_code = models.CharField(max_length=16)
    city = models.CharField(max_length=64)
    pob = models.CharField(max_length=32)
    street = models.CharField(max_length=64)
    house = models.CharField(max_length=32)

    def __str__(self):
        return self.state
