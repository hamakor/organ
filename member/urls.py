from django.conf.urls import url

from .views import IndexView, AddView, ItemView, EditView

app_name = 'member'
urlpatterns = [
    url(r'^$', IndexView.as_view(), name='index'),
    url(r'^add/$', AddView.as_view(), name='add'),
    url(r'^(?P<pk>\d+)/$', ItemView.as_view(), name='item'),
    url(r'^(?P<pk>\d+)/edit/$', EditView.as_view(), name='edit'),
]
