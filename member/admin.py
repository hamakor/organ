from django.contrib import admin

from .models import (
    Member,
    MembershipType,
    Membership,
    MemberEmail,
    MemberPhone,
    MemberAddress,
)


@admin.register(Member)
class MemberAdmin(admin.ModelAdmin):
    list_display = ('member_id', 'first_name', 'last_name', 'user')
    search_fields = ['member_id', 'first_name', 'last_name', 'user__email', 'id_number']


@admin.register(MembershipType)
class MembershipTypeAdmin(admin.ModelAdmin):
    list_display = ('name', 'period')


@admin.register(Membership)
class MembershipAdmin(admin.ModelAdmin):
    list_display = ('member', 'membership_type', 'start_date', 'end_date')
    search_fields = ['member__first_name', 'member__last_name']


@admin.register(MemberEmail)
class MembershipAdmin(admin.ModelAdmin):
    list_display = ('member', 'email')
    search_fields = ['member__first_name', 'member__last_name', 'email']


@admin.register(MemberPhone)
class MembershipAdmin(admin.ModelAdmin):
    list_display = ('member', 'phone', 'text_only')
    search_fields = ['member__first_name', 'member__last_name', 'phone']


@admin.register(MemberAddress)
class MembershipAdmin(admin.ModelAdmin):
    list_display = ('member', 'house', 'street', 'pob', 'city', 'zip_code', 'state')
    search_fields = ['member__first_name', 'member__last_name']


