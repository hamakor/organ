from django.contrib.auth.mixins import LoginRequiredMixin
from django.urls import reverse
from django.views.generic import ListView, CreateView, DetailView, UpdateView
from djext.blame.views import BlameFormView

from member.models import MembershipType
from .forms import MemberForm, MemberPhoneForm, MemberEmailForm
from .models import Member


class IndexView(LoginRequiredMixin, ListView):
    model = Member


class AddView(LoginRequiredMixin, BlameFormView, CreateView):
    model = Member
    form_class = MemberForm
    template_name_suffix = '_create_form'

    def get_success_url(self):
        if self.object:
            return reverse('member:item', kwargs={'pk': self.object.id})
        else:
            return reverse('member:index')


class ItemView(LoginRequiredMixin, DetailView):
    model = Member

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        context['phone_form'] = MemberPhoneForm(member=self.object, user=self.request.user)
        context['email_form'] = MemberEmailForm(member=self.object, user=self.request.user)

        membership_types = MembershipType.objects.all()
        context['membership_types'] = membership_types

        return context


class EditView(LoginRequiredMixin, BlameFormView, UpdateView):
    model = Member
    form_class = MemberForm
    template_name_suffix = '_update_form'

    def get_success_url(self):
        return reverse('member:item', kwargs={'pk': self.object.id})
