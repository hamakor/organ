from djext.blame.forms import BlameForm

from .models import Member, Membership, MemberEmail, MemberPhone, MemberAddress


class MemberForm(BlameForm):
    class Meta:
        model = Member
        fields = ['member_id', 'first_name', 'last_name', 'id_type', 'id_number', 'locale']


class MemberFromUserForm(BlameForm):
    def save(self, commit=True):
        self.instance.user = self.user
        self.instance.first_name = self.user.first_name
        self.instance.last_name = self.user.last_name
        return super().save(commit)

    class Meta:
        model = Member
        fields = ['member_id', 'id_type', 'id_number', 'locale']


class MemberSubFormMixin(object):
    def __init__(self, *args, **kwargs):
        self.member = kwargs.pop('member')
        super().__init__(*args, **kwargs)

    def save(self, commit=True):
        self.instance.member = self.member
        return super().save(commit)


class MembershipForm(MemberSubFormMixin, BlameForm):
    class Meta:
        model = Membership
        fields = ['membership_type', 'start_date', 'end_date']


class MemberEmailForm(MemberSubFormMixin, BlameForm):
    class Meta:
        model = MemberEmail
        fields = ['email']


class MemberPhoneForm(MemberSubFormMixin, BlameForm):
    class Meta:
        model = MemberPhone
        fields = ['phone', 'text_only']


class MemberAddressForm(MemberSubFormMixin, BlameForm):
    class Meta:
        model = MemberAddress
        fields = ['street', 'house', 'pob', 'city', 'zip_code', 'state']

